package ru.konovalov.tm.api;

import ru.konovalov.tm.model.AbstractOwner;

public interface IOwnerService <E extends AbstractOwner> extends IService<E>,IOwnerRepository<E>{
}
