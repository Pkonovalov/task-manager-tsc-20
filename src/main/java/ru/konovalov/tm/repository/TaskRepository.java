package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll(final String userId, Comparator<Task> comparator) {
        final List<Task> taskList = new ArrayList<>(findAll(userId));
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> findAll(final String userId) {
        List<Task> list = new ArrayList<>();
        for (Task task : this.tasks) {
            if (userId.equals(task.getUserId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findALLTaskByProjectId(final String userId, final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = findALLTaskByProjectId(userId, projectId);
        tasks.removeAll(listByProject);
        return listByProject;
    }

    @Override
    public Task assignTaskByProjectId(final String userId, final String projectId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unassignTaskByProjectId(final String userId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(final String userId, Task task) {
        List<Task> list = findAll(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        List<Task> list = findAll(userId);
        tasks.remove(task);
    }

    @Override
    public void clear(final String userId) {
        List<Task> list = findAll(userId);
        this.tasks.removeAll(list);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        List<Task> list = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        for (int i = tasks.size(); i-- > 0; ) {
            if (projectId.equals(tasks.get(i).getProjectId()) && userId.equals(tasks.get(i).getUserId())) {
                tasks.remove(i);
            }
        }
    }

    @Override
    public int size(final String userId) {
        List<Task> list = findAll(userId);
        return tasks.size();
    }

    @Override
    public boolean existsById(final String userId, final String projectId) {
        for (final Task task : tasks) {
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }


    @Override
    public boolean existsByName(final String userId, final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }

    @Override
    public String getIdByIndex(int index) {
        return tasks.get(index).getId();
    }

}








