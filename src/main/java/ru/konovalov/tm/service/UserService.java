package ru.konovalov.tm.service;

import ru.konovalov.tm.api.repository.IUserRepository;
import ru.konovalov.tm.api.service.IUserService;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.exeption.empty.EmptyEmailException;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyLoginException;
import ru.konovalov.tm.exeption.empty.EmptyPasswordException;
import ru.konovalov.tm.exeption.entity.EmailExistException;
import ru.konovalov.tm.exeption.entity.LoginExistException;
import ru.konovalov.tm.exeption.entity.UserNotFoundException;
import ru.konovalov.tm.exeption.user.EmailExistsException;
import ru.konovalov.tm.exeption.user.LoginExistsException;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.HashUtil;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeUserByLogin(login);
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public User add(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User add(final String login, final String password, final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new EmailExistException(login);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        if (userRepository.existsByEmail(email)) throw new EmailExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    public boolean existsByLogin(final String login) {
        return userRepository.existsByLogin(login);
    }

}
