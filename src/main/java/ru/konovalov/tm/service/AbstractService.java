package ru.konovalov.tm.service;

import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.api.IService;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public void add(final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    public E removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
        return null;
    }


}
