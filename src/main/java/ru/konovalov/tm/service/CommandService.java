package ru.konovalov.tm.service;

import ru.konovalov.tm.api.repository.ICommandRepository;
import ru.konovalov.tm.api.service.ICommandService;
import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.model.Command;

import java.util.Collection;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;


public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        if (isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        if (isEmpty(arg)) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
