package ru.konovalov.tm.exeption.empty;

import ru.konovalov.tm.exeption.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty...");
    }

}
