package ru.konovalov.tm.command.system;

import ru.konovalov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.name() + ": " + command.description());
        }
    }
}