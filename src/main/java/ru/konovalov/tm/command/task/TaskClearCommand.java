package ru.konovalov.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand{
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Clear all tasks";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASKS ClEAR]");
        serviceLocator.getTaskService().clear(userId);
    }
}
