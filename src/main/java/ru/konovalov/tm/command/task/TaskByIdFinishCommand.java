package ru.konovalov.tm.command.task;

import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

public class TaskByIdFinishCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
    }

}

